# HDRSharpLib

## Description
1. HDRSharpLib is a FREE, OpenSource .NET library for reading and writing Radiance HDR files.
2. Library contain only 1 file: "HDRSharpLib.cs", that's why it can be used in any standalone project!

## Link
* https://gitlab.com/Alex_Green/HDRSharpLib

## License:
**MIT**

## Supported features:
1. Bitmap, Byte[], File, Stream <-> HDR.
2. Supported: RGB images only (XYZ images are not supported).
3. Only "New RLE" supported. Old RLE encoding stopped usage a lot of time ago.
4. Allow Read (Uncompressed, New RLE) and write (Uncompressed) HDR images.

## Author
Zelenskyi Alexandr (Зеленський Олександр)
If you will found some bugs, please write me: alex.green.zaa.93@gmail.com