﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace HDRSharpLib
{
    public partial class Form1 : Form
    {
        string[] Files = Directory.GetFiles(@"..\..\..\HDR File Examples\", "*.hdr", SearchOption.AllDirectories);
        HDR Image;

        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < Files.Length; i++)
                listBox1.Items.Add(Path.GetFileName(Files[i]));
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
                return;
            string Image_File = Files[listBox1.SelectedIndex];
            if (File.Exists(Image_File))
            {
                Image = new HDR(Image_File);
                ShowImage();
            }
        }

        private void buttonTryConvert_Click(object sender, EventArgs e)
        {
            if (Image == null)
                return;

            Image = (HDR)((Bitmap)Image);
            ShowImage();
        }

        private void buttonSaveSelected_Click(object sender, EventArgs e)
        {
            if (Image == null)
                return;

            string OutDir = @"D:\HDR\";
            if (!Directory.Exists(OutDir))
                Directory.CreateDirectory(OutDir);

            Image.Save(Path.Combine(OutDir, Path.GetFileName("_____.hdr")));
        }

        private void buttonSaveAll_Click(object sender, EventArgs e)
        {
            string OutDir = @"D:\HDR\";
            if (!Directory.Exists(OutDir))
                Directory.CreateDirectory(OutDir);

            for (int i = 0; i < Files.Length; i++)
                new HDR(Files[i]).Save(Path.Combine(OutDir, Path.GetFileName(Files[i])));
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) && (e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
                e.Effect = DragDropEffects.Move;
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) && e.Effect == DragDropEffects.Move)
            {
                string[] Files = (string[])e.Data.GetData(DataFormats.FileDrop);
                Image = HDR.FromFile(Files[0]);
                ShowImage();
            }
        }

        void ShowImage()
        {
            pictureBox1.Image = (Bitmap)Image;
            richTextBox1.Text = Image.GetInfo();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
            else if (e.KeyCode == Keys.F1)
            {
                if (Image != null)
                {
                    Image = HDR.FromBytes(Image.ToBytes());
                    ShowImage();
                }
            }
        }
    }
}
